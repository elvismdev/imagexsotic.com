<div class="wrap">
<div id="icon-edit" class="icon32"><br></div>
<h2>Image Grabber</h2>

<?php if ($__message): ?>
<div class="updated fade below-h2" id="message" style="background-color: rgb(255, 251, 204);"><p><?php echo $__message; ?></p></div><br />
<?php endif; ?>

<br class="clear" />
<form class="frm-search" method="get" action="<?php #echo admin_url('edit.php?page=image-grab'); ?>">
	<label>Keyword</label> <input type="text" name="q" value="<?php echo (isset($q) ? $q : '');?>" />
	<select name="filter">
		<option></option>
		<option <?php echo (isset($filter) && $filter == 'Small' ? 'selected="selected"' : '');?>>Small</option>
		<option <?php echo (isset($filter) && $filter == 'Medium' ? 'selected="selected"' : '');?>>Medium</option>
		<option <?php echo (isset($filter) && $filter == 'Large' ? 'selected="selected"' : '');?>>Large</option>
	</select>
	<input type="submit" name="submit" value="Search Image" class="button-primary" />
	<input type="hidden" name="page" value="image-grab" />
	<input type="hidden" name="skip" value="0" />
</form>

<?php 
if (isset($results)): 
	echo '<form method="post" id="img-form2">';
	echo '<div class="nav-panel">';
	if ($skip) 
		echo '<a href="'.$prev.'" class="button prev">Previous Results</a> ';
	echo '<a href="'.$next.'" class="button next">Next Results</a> ';
	echo '<input type="submit" name="submit" value="Next Step" class="button-primary next" /></div>';
	echo '<ul id="resultList">';

   foreach($results->d->results as $id => $value) 
   {                        
   	echo '<li class="resultlistitem" title="'.$value->Title.'">
   		<input type="checkbox" id="item-'.$id.'" name="checked[]" value="'.$id.'" />
   		<label for="item-'.$id.'">
				<div class="img_c"><img src="'. $value->Thumbnail->MediaUrl .'" /></div>
				<div class="dimension">'.$value->Width.'x'.$value->Height.' &bull; '.format_bytes($value->FileSize).'</div>
   		</label>
   		<input type="hidden" name="title['.$id.']" value="'.$value->Title.'" />
   		<input type="hidden" name="image_url['.$id.']" value="'.$value->MediaUrl.'" />
   		<input type="hidden" name="thumb_url['.$id.']" value="'.$value->Thumbnail->MediaUrl.'" />
   		<input type="hidden" name="dimension['.$id.']" value="'.$value->Width.'x'.$value->Height.'" />
   		<input type="hidden" name="file_size['.$id.']" value="'.format_bytes($value->FileSize).'" />
   	</li>';
   }

   echo "</ul>";
	echo '<div class="nav-panel">';
	if ($skip) 
		echo '<a href="'.$prev.'" class="button prev">Previous Results</a> ';
	echo '<a href="'.$next.'" class="button next">Next Results</a> ';
	echo '<input type="submit" name="submit" value="Next Step" class="button-primary next" /></div>';
   echo '<input type="hidden" name="act" value="step2" />';
   echo '</form>';
   
endif;

function format_bytes($size) {
    $units = array(' B', ' KB', ' MB', ' GB', ' TB');
    for ($i = 0; $size >= 1024 && $i < 4; $i++) $size /= 1024;
    return round($size, 2).$units[$i];
}
?>

</div>
<script type="text/javascript">
//<![CDATA[
jQuery(function($){
	$('#img-form2').submit(function() {
		var ln = $('input[type="checkbox"]:checked').length;
		if (ln < 1) {
			alert('Please select one or more images!');
			return false;
		}
	});
});
//]]>
</script>
