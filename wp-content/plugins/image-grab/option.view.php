<div class="wrap">
<div id="icon-options-general" class="icon32"><br></div>
<h2>Image Grabber Option</h2>

<?php if ($__message): ?>
<div class="updated fade below-h2" id="message" style="background-color: rgb(255, 251, 204);"><p><?php echo $__message; ?></p></div><br />
<?php endif; ?>

<br class="clear" />
<form action="" method="post">

<table class="form-table">
	<tbody>
		<tr class="form-field form-required">
			<th valign="top" scope="row"><label for="api">Azure Account ID</label></th>
         <td><input type="text" aria-required="true" size="40" value="<?php echo $data['azure_api_key'];?>" id="api" name="azure_api_key">
         <p class="description"><i>Azure Account ID. <a href="http://datamarket.azure.com/dataset/bing/search" target="_blank">Get Azure ID</a></i></p></td>
		</tr>
		
		<tr><th colspan="2"><h3>Bulk Posts</h3></th></tr>
		
		<tr>
			<th><label for="interval">Default Schedule Interval</label></th>
			<td><input type="text" value="<?php echo $data['interval'];?>" id="interval" name="interval" size="10"> minutes.</td>
		</tr>
		
		<tr>
		  <th>Default Category</th>
		  <td>
			 <?php echo $this->_buildCategoryOptionList('dafault_category[]', $data['default_category'], 'default_category', $data['multiple_category']);?><br />
			 <input type="checkbox" name="multiple_category" id="multiple" value="1" <?php echo ($data['multiple_category'] == 1 ? 'checked="checked"': '');?> /><label for="multiple">Multiple </label>
		  </td>
		</tr>

		<tr class="form-field form-required">
			<th valign="top" scope="row"><label for="content">Content Description</label></th>
         <td><textarea id="content" name="content_desc" rows="4"><?php echo $data['content_desc'];?></textarea>
         	<p class="description">Default format for content description<br />Tag: {TITLE}, {DIMENSION}, {FILE_SIZE}</p>
         	<input type="radio" name="content_pos" id="content_pos_1" value="before" <?php echo ($data['content_pos'] == 'before' ? 'checked="checked"': '');?> /><label for="content_pos_1">Before Image</label>&nbsp;
         	<input type="radio" name="content_pos" id="content_pos_2" value="after" <?php echo ($data['content_pos'] == 'after' ? 'checked="checked"': '');?> /><label for="content_pos_2">After Image</label>
         </td>
		</tr>
	</tbody>
</table>

<p class="submit"><input type="submit" class="button-primary" value="Save Changes" name="save">
<input type="hidden" name="action" value="update" />
</p>
</form>

</div>
<script type="text/javascript">
//<![CDATA[
jQuery(function($){
  $('#multiple').change(function() {
    if ($(this).attr("checked") == true) {
      $('#default_category').attr('multiple', 'multiple');
      $('#default_category').attr('size', '10');
    } else {
      $('#default_category').removeAttr('multiple');
      $('#default_category').removeAttr('size');
    }
  })
});
//]]>
</script>
