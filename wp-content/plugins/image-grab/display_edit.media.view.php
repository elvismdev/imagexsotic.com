<?php
media_upload_header();
if ( !function_exists("curl_init") && !ini_get("allow_url_fopen") ) {
	echo '<div id="message" class="error"><p><b>cURL</b> or <b>allow_url_fopen</b> needs to be enabled. Please consult your server Administrator.</p></div>';
} elseif ( $error ) {
	echo $error;
} else {
	if ( $fileSaved && $attach_ids ) {
		echo '<div id="message" class="updated"><p>File saved.</p></div>';
	}
}
?>
<div class="media-content">
<h3>Edit Image</h3>

<?php if ($data): ?>
<form method="post">
<div class="nav-panel"><input type="submit" name="submit" value="Grab & Save Images" class="button-primary next" /></div>
<ul class="listData">
<?php foreach($data as $row): ?>
<?php
$parts = parse_url($row->image_url);  
$file_name = basename($parts['path']);
?>
	<li>
		<div class="img_t"><img src="<?php echo $row->thumb_url;?>" /></div>
		<div class="edit_t">
			<label for="filename_<?php echo $row->id;?>">File Name</label>
			<input type="text" id="filename_<?php echo $row->id;?>" name="data[<?php echo $row->id;?>][filename]" value="<?php echo $file_name;?>" />
			<label for="title_<?php echo $row->id;?>">Title</label>
			<input type="text" id="title_<?php echo $row->id;?>" name="data[<?php echo $row->id;?>][title]" value="<?php echo $row->title;?>" />
			<label for="alt_<?php echo $row->id;?>">Alternate</label>
			<input type="text" id="alt_<?php echo $row->id;?>" name="data[<?php echo $row->id;?>][alt]" value="" />
			<label for="caption_<?php echo $row->id;?>">Caption</label>
			<textarea name="data[<?php echo $row->id;?>][caption]" id="caption_<?php echo $row->id;?>" rows="2"></textarea>
			<input type="hidden" name="data[<?php echo $row->id;?>][id]" value="<?php echo $row->id;?>" />
			<input type="hidden" name="data[<?php echo $row->id;?>][image_url]" value="<?php echo $row->image_url;?>" />
		</div>
	</li>
<?php endforeach;?> 
</ul>
<div class="nav-panel"><input type="submit" name="submit" value="Grab & Save Images" class="button-primary next" /></div>
<input type="hidden" name="act" value="save" />
</form>
<?php endif; ?>
</div>
