<?php
media_upload_header();
if ( !function_exists("curl_init") && !ini_get("allow_url_fopen") ) {
	echo '<div id="message" class="error"><p><b>cURL</b> or <b>allow_url_fopen</b> needs to be enabled. Please consult your server Administrator.</p></div>';
} elseif ( $error ) {
	echo $error;
} else {
	if ( $fileSaved && $attach_ids ) {
		echo '<div id="message" class="updated"><p>File saved.</p></div>';
	}
}
?>
<div class="media-content">
<form action="" method="post" id="image-form" class="media-upload-form type-form">
	<label>Keyword</label> <input type="text" name="q" value="<?php echo (isset($q) ? $q : '');?>" />
	<select name="filter">
		<option></option>
		<option <?php echo (isset($filter) && $filter == 'Small' ? 'selected="selected"' : '');?>>Small</option>
		<option <?php echo (isset($filter) && $filter == 'Medium' ? 'selected="selected"' : '');?>>Medium</option>
		<option <?php echo (isset($filter) && $filter == 'Large' ? 'selected="selected"' : '');?>>Large</option>
	</select>
	<input type="submit" name="submit" value="Search Image" class="button-primary" />
	<input type="hidden" name="act" value="search" />
	<input type="hidden" name="skip" value="0" />
</form>

<?php 
if (isset($results)): 
	echo '<form method="post" id="img-form2">';
	echo '<div class="nav-panel">';
	if ($skip) 
		echo '<a href="#" data-skip="'.$prev.'" class="button prev">Previous Results</a> ';
	echo '<a href="#" data-skip="'.$next.'" class="button next">Next Results</a> ';
	echo '<input type="submit" name="submit" value="Next Step" class="button-primary next" /></div>';
	echo '<ul id="resultList">';

   foreach($results->d->results as $id => $value) 
   {                        
   	echo '<li class="resultlistitem" title="'.$value->Title.'">
   		<input type="checkbox" id="item-'.$id.'" name="checked[]" value="'.$id.'" />
   		<label for="item-'.$id.'">
				<div class="img_c"><img src="'. $value->Thumbnail->MediaUrl .'" /></div>
				<div class="dimension">'.$value->Width.'x'.$value->Height.' &bull; '.format_bytes($value->FileSize).'</div>
   		</label>
   		<input type="hidden" name="title['.$id.']" value="'.$value->Title.'" />
   		<input type="hidden" name="image_url['.$id.']" value="'.$value->MediaUrl.'" />
   		<input type="hidden" name="thumb_url['.$id.']" value="'.$value->Thumbnail->MediaUrl.'" />
   	</li>';
   }

   echo "</ul>";
	echo '<div class="nav-panel">';
	if ($skip) 
		echo '<a href="#" data-skip="'.$prev.'" class="button prev">Previous Results</a> ';
	echo '<a href="#" data-skip="'.$next.'" class="button next">Next Results</a> ';
	echo '<input type="submit" name="submit" value="Next Step" class="button-primary next" /></div>';
   echo '<input type="hidden" name="act" value="step2" />';
   echo '</form>';
   
endif;

function format_bytes($size) {
    $units = array(' B', ' KB', ' MB', ' GB', ' TB');
    for ($i = 0; $size >= 1024 && $i < 4; $i++) $size /= 1024;
    return round($size, 2).$units[$i];
}
?>
</div>
<script type="text/javascript">
//<![CDATA[
jQuery(function($){
	$('#img-form2').submit(function() {
		var ln = $('input[type="checkbox"]:checked').length;
		if (ln < 1) {
			alert('Please select one or more images!');
			return false;
		}
	});
	
	$('.nav-panel a.next, .nav-panel a.prev').click(function() {
		var skip = $(this).data('skip');
		
		$('#image-form input[name="skip"]').val(skip);
		$('#image-form input[name="submit"]').click();
				
		return false;
	});
});
//]]>
</script>
