<?php
	//if uninstall not called from WordPress exit
	if ( !defined( 'WP_UNINSTALL_PLUGIN' ) )
		exit();

	global $wpdb;

	delete_option( "102drive_nofollow_external_links" );

	$wpdb->query( 'DROP TABLE 102drive_urls' );
?>