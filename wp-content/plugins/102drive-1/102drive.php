<?php
/*
  Plugin Name: 102Drive Tools
  Plugin URI: -
  Description: 102Drive Tools for Wordpress
  Version: 2.0
  Author: Reinier G. Menendez (Soft4Good Solutions)
  Author URI: http://www.soft4good.com
*/

// Install

function init_102drive( $content )
{
  if ( isset( $_GET['activate'] ) && $_GET['activate'] == 'true' )
  { 
    global $wpdb;
  
	  $fpSQL = fopen( __DIR__ . '/db_all.sql', 'r' );
	  $sQueries = fread( $fpSQL, filesize( __DIR__ . '/db_all.sql' ) );
	  fclose( $fpSQL );

	  $aQueries = explode( ';', $sQueries );

	  // FIX THIS:
	  $bSQLOk = true;
	  foreach ( $aQueries as $sQuery )
		  $bSQLOk &= mysql_query( $sQuery );
/*
	  if ( !$bSQLOk )
		  echo( "ERROR: Can't create database tables or can't apply some database changes, please check your setup..." );
*/
  }
}

// General Settings

function set_102drive_options()
{
  add_option( "102drive_nofollow_external_links", "0" );
}

// Administrator Backend Menu

function modify_menu_for_102drive()
{
  add_menu_page( 'General Settings' , '102Drive Tools', 'manage_options', '102drive-tools', 'general_settings_102drive', WP_PLUGIN_URL . '/102drive/img/wsc_logo.png' );

	add_submenu_page( '102drive-tools' , 'General Settings', 'General Settings', 'manage_options', '102drive-tools', 'general_settings_102drive' );
	add_submenu_page( '102drive-tools', '102Drive URL Tweaks', 'URL Tweaks', 'manage_options', '102drive-url-tweaks', 'url_tweaks_102drive' );
	add_submenu_page( 'Add URLs', 'Add URLs', 'Add URLs', 'manage_options', '102drive-url-tweaks-add', 'url_tweaks_add_102drive' );
}

// Administrator backend pages

require_once('includes/admin/general.php');
require_once('includes/admin/urls.php');

function admin_scripts_102drive()
{
  echo '<script src="'.WP_PLUGIN_URL.'/102drive/js/admin_utils.js" type="text/javascript"></script>';
  echo '<link href="'.WP_PLUGIN_URL.'/102drive/css/admin_styles.css" rel="stylesheet" type="text/css" />';
}

$o102DriveURL = null;

function scripts_102drive()
{
	global $o102DriveURL;

	if ( $o102DriveURL && $o102DriveURL->noindex )
		echo '<meta name="robots" content="noindex" />';
}

function send_headers_102drive()
{
	global $wp_query;
	global $wpdb;
	global $o102DriveURL;

	$sRequestURL = $_SERVER['REQUEST_URI'];
	$sQuery = "SELECT * FROM 102drive_urls WHERE url = '$sRequestURL' LIMIT 1";
	$a102DriveURL = $wpdb->get_results( $sQuery );
	$o102DriveURL = $a102DriveURL[0];

	if ( $o102DriveURL )
	{
		if ( $o102DriveURL->do_410 )
		{

			$wp_query->set_404();
			status_header( 410 );
			get_template_part( 404 );
			exit();
		}
		elseif ( $o102DriveURL->do_404 )
		{
			$wp_query->set_404();
			status_header( 404 );
			get_template_part( 404 );
			exit();
		}
		elseif ( $o102DriveURL->do_301 )
			exit( wp_redirect( $o102DriveURL->redirect_url, 301 ) );
		elseif ( $o102DriveURL->do_302 )
			exit( wp_redirect( $o102DriveURL->redirect_url, 302 ) );
	}

}

function content_filter_102drive( $sContent )
{
	$bNoFollowExternalLinks = get_option( '102drive_nofollow_external_links' ) == '1' ? true : false;
	if ( $bNoFollowExternalLinks )
	{
		preg_match_all( '/<a [^>]+?>/s', $sContent, $aLinkMatches );

		foreach( $aLinkMatches[0] as $sOrgLink )
		{
			$sLink = strtolower( $sOrgLink );
			if ( strpos( $sLink, 'http' ) && strpos( $sLink, 'http://www.commonsensewithmoney.com' ) === false && strpos( $sLink, 'http://commonsensewithmoney.com' ) === false && strpos( $sLink, 'nofollow' ) === false )
			{
				$sLink = str_replace( '<a', '<a rel="nofollow"', $sLink );
				$sContent = str_replace( $sOrgLink, $sLink, $sContent );
			}
		}
	}

	return $sContent;
}

// HOOKS

add_filter( 'init', 'init_102drive' );
add_filter( 'the_content', 'content_filter_102drive' );

add_action( 'send_headers', 'send_headers_102drive' );

add_action( 'admin_menu', 'modify_menu_for_102drive' );
add_action('wp_head', 'scripts_102drive');
add_action('admin_head', 'admin_scripts_102drive');

register_activation_hook( __FILE__, "set_102drive_options" );

?>