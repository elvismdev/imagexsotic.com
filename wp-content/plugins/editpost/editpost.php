<?php

/*
Plugin Name: EDITPOST
Description: Edit a post inserting CUSTOM DATA before it saves
Version: 1.0
Author: Elvis Morales
Author URI: http://www.linkedin.com/in/elvismdev
 */

define('EDITPOST_VERSION', '0.1');
define('EDITPOST_PLUGIN_URL', plugin_dir_url( __FILE__ ));

add_filter('wp_insert_post_data', 'mycustom_data_filter', 10, 2);

function mycustom_data_filter($filtered_data, $raw_data){

    if (strstr($filtered_data['post_content'], 'as your desktop computer wallpaper now'))
        return $filtered_data;

    $category = get_category( $raw_data['post_category'][1] );
    $randompost = get_posts ( array( 'posts_per_page'   => 1, 'orderby' => 'rand' ));

    $content = '<br>Set '.$filtered_data['post_title'].' as your desktop computer wallpaper now. This free computer background is in hd and a top choice from our <a href="'.get_category_link($category->term_id).'">'.$category->name.'</a> section. We have '.$filtered_data['post_title'].' in file type form. You can download this wallpaper to your desktop, laptop or tablet and set as your background. Don\'t forget to check out other wallpapers and backgrounds in our <a href="'.get_category_link($category->term_id).'">'.$category->name.'</a>, maybe you will like something like <a href="'.$randompost[0]->guid.'">'.$randompost[0]->post_title.'</a>!';

    $filtered_data['post_content'] .= $content;

    return $filtered_data;
}

?>
