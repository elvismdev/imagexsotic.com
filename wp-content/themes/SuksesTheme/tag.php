<?php get_header(); ?>	 	 
<div class="isi_postingan">
<?php $x = 0; 
while (have_posts()) : the_post(); ?> 
<div class="home_post_box"> 
<a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_post_thumbnail('featured-home'); ?></a>
<!--img src="<!?php echo get_freestyle_image(); ?>" height="140" width="228"/></a-->
<div class="infopost">
<h2><?php echo substr(get_the_title(),0,40); ?></h2>
<?php
$img_id = get_post_thumbnail_id($post->ID);
$image = wp_get_attachment_image_src($img_id, $optional_size);
?>
<p><?php echo '' . ($image[1]) . 'x' . ($image[2]); ?> px | <?php echo getPostViews(get_the_ID()); ?> Views</p>
</div>
<div class="clearfix"></div>
</div><!--//big_post_box--> 
<?php $x++; ?>
<?php endwhile; ?>
<div class="clear"></div>
<div id="post-navigator">
<?php if (function_exists('custom_wp_pagenavi')) : ?>
<?php custom_wp_pagenavi(); ?>
<?php else : ?>
<div class="alignleft"><?php posts_nav_link('',__('&laquo; Newer Posts'),'') ?></div>
<div class="alignright"><?php posts_nav_link('','',__('Older Posts &raquo;')) ?></div>
<?php endif; ?>
<div class="clearfix"></div>
</div>	  
</div><!--//home_post_box-->   
<?php get_sidebar(''); ?>  
<div class="clear"></div>
<?php get_footer(); ?>    