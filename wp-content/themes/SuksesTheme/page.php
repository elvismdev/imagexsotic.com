<?php get_header(); ?>
<div class="single_page">
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
<h1><?php the_title(); ?></h1>
<?php the_content(); ?>
<br /><br />
<?php endwhile; else: ?>
<h3>Sorry, no posts matched your criteria.</h3>          
<?php endif; ?>      
</div><!--//single_page-->
<?php custom_wp_pagenavi(); ?>
<?php get_sidebar(); ?>   
<div class="clear"></div>
<?php get_footer(); ?>    