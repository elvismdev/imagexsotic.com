<?php //randompost
query_posts(array('orderby' => 'rand', 'showposts' => 8));
if (have_posts()) :
while (have_posts()) : the_post(); ?>
<div class="home_post_box">
<a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_post_thumbnail('featured-home'); ?></a>
<div class="infopost">
<h2><?php echo substr(get_the_title(),0,40); ?></h2>
<?php
$img_id = get_post_thumbnail_id($post->ID);
$image = wp_get_attachment_image_src($img_id, $optional_size);
?>
<p><?php echo '' . ($image[1]) . 'x' . ($image[2]); ?> px | <?php echo getPostViews(get_the_ID()); ?> Views</p>
</div>
</div>
<?php endwhile;
endif; ?>