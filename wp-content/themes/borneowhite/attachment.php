<?php get_header(); ?>
<div id="attachment">
<div class="single_attachment">
<h1 class="lead2"><?php the_title() ?> In Full Size</h1>
<div class="attachment_page">
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
<?php echo wp_get_attachment_image( $post->ID, 'large' ); ?>               
<br />	
<?php endwhile; else: ?>           
<h3>Sorry, no posts matched your criteria.</h3>          
<?php endif; ?>
</div>		
</div>		
<div class="clear"></div>	
<div class="random_post">						
<?php //randompost
query_posts(array('orderby' => 'rand', 'showposts' => 8));
if (have_posts()) :
while (have_posts()) : the_post(); ?>
<div class="home_post_box">
<a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_post_thumbnail('featured-home'); ?></a>
<div class="infopost">
<h2><strong><?php echo substr(get_the_title(),0,40); ?></strong></h2>
<?php
$img_id = get_post_thumbnail_id($post->ID);
$image = wp_get_attachment_image_src($img_id, $optional_size);
?>
<p><?php echo '' . ($image[1]) . 'x' . ($image[2]); ?> px | <?php echo getPostViews(get_the_ID()); ?> Views</p>
</div>
</div> 

<?php endwhile;
endif; ?>
<div class="clear"></div>
</div>
</div> 
<?php get_footer(); ?>