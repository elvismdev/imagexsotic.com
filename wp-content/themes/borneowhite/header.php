<!DOCTYPE html>
<html>
<head> 
<meta name="google-site-verification" content="JKDg7tG87YWDoEhiCIsuaIu7BK8SKYEgyzWduJuJ7uc" />
      <?php include (TEMPLATEPATH . '/meta.php'); ?>   
  <link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css" media="screen" title="no title" charset="utf-8"/>
  <?php wp_head(); ?>
</head>
<body>
    <?php $shortname = "photographer"; ?>
    <?php if(get_option($shortname.'_custom_background_color','') != "") { ?>
    <style type="text/css">
      body { background-color: <?php echo get_option($shortname.'_custom_background_color',''); ?>; }
    </style>
    <?php } ?>
    <style type="text/css">
    body {
      <?php if(stripslashes(stripslashes(get_option($shortname.'_header_background_image',''))) != "") { ?> 
      background: url('<?php echo stripslashes(stripslashes(get_option($shortname.'_header_background_image',''))); ?>') <?php echo get_option($shortname.'_header_background_repeat','no-repeat') ?>; 
      background-position: <?php echo get_option($shortname.'_header_background_vertical','center') . ' ' . get_option($shortname.'_header_background_horizontal','center'); ?>;
      <?php } ?>    
    }    
    </style>
<div id="main_container">
    <div id="header">
        <a href="<?php bloginfo('url'); ?>"><img src="<?php echo stripslashes(stripslashes(get_option($shortname.'_custom_logo_url',get_bloginfo('stylesheet_directory') . '/images/logo.png'))); ?>" class="logo" /></a>
<!--        <ul>
          <li><a href="#">Home</a></li>
          <li><a href="#">About</a></li>
          <li><a href="#">Blog</a></li>
          <li><a href="#">Contact</a></li>
        </ul>-->
        <div class="clear"></div>
    </div><!--//header-->
    <div class="featured_text"><?php echo stripslashes(stripslashes(get_option($shortname.'_featured_text',''))); ?></div>