<?php get_header(); ?>	 	 
<div class="isi_postinganctg">
<?php if (is_paged()){ }
while (have_posts()) : the_post(); ?> 
<div class="home_post_box"> 
<a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_post_thumbnail('featured-home'); ?></a>
<div class="infopost">
<h2><strong><?php echo substr(get_the_title(),0,40); ?></strong></h2>
<?php
$img_id = get_post_thumbnail_id($post->ID);
$image = wp_get_attachment_image_src($img_id, $optional_size);
?>
<p><?php echo '' . ($image[1]) . 'x' . ($image[2]); ?> px | <?php echo getPostViews(get_the_ID()); ?> Views</p>
</div>
</div> 
<?php endwhile; ?>
</div><!--//home_post_box--> 
<?php get_sidebar(); ?>
<div class="clear"></div>
      <div class="navigation">
          <?php if(get_previous_posts_link()) { ?><div class="left"><?php previous_posts_link('&laquo; Previous') ?></div><?php } ?>
          <?php if(get_next_posts_link()) { ?><div class="right"><?php next_posts_link('Next &raquo;') ?></div><?php } ?>
          <div class="clear"></div>
 </div>  
<div class="clear"></div>
<?php get_footer(); ?>    