<?php get_header(); ?>
<div id="content">
<div class="single_content">
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>      
<h1 class="lead"><?php the_title() ?> Wallpaper</h1>
<div class="iklan">

</div>
<div class="image_content">
<center>
<?php
$argsThumb = array(
'order'          => 'ASC',
'post_type'      => 'attachment',
'post_parent'    => $post->ID,
'post_mime_type' => 'image',
'post_status'    => null
);
$attachments = get_posts($argsThumb);
$thumb_title = get_the_title( $thumb_id );
if ($attachments) {
foreach ($attachments as $attachment) {
//echo apply_filters('the_title', $attachment->post_title);
echo '<img src="' . wp_get_attachment_url($attachment->ID, 'large') . '" alt="' . $thumb_title . ' Wallpaper" title="' . $thumb_title . ' Wallpaper" />'; } }
?>
<!--h2><strong><?php the_title(); ?> Wallpaper</strong></h2-->          
<?php endwhile; else: ?>
<h3>Sorry, no posts matched your criteria.</h3>          
<?php endif; ?>  
<?php setPostViews(get_the_ID()); ?>
</center>
<div class="clear"></div> 
<h2><strong><?php the_title(); ?> | <?php global $wpdb; $attachment_id = $wpdb->get_var("SELECT ID FROM $wpdb->posts WHERE
post_parent = '$post->ID' AND post_status = 'inherit' AND post_type='attachment' ORDER BY post_date DESC LIMIT 1"); ?>
<?php 
$attachments = get_posts($args);
$image_attributes = wp_get_attachment_image_src( $attachment_id,'full' ); // returns an array
?> 
<?php echo $image_attributes[1] ?> x <?php echo $image_attributes[2] ?> Pixel</strong></h2>
</div>
<div class="adsen">
			
</div>
<div id="deskripsi">
<div class="description">
<h3>Description from <?php the_title(); ?> Wallpaper :</h3>
<p><strong><?php the_title(); ?></strong>, Download this wallpaper for free in high resolution. 
<i><?php the_title(); ?></i> was posted in <?php the_time('F j, Y'); ?> at <?php the_time('g:i a'); ?> and
This <u><?php the_title(); ?> Wallpaper </u> has viewed by <?php setPostViews(get_the_ID()); ?><?php echo getPostViews(get_the_ID()); ?> 
users.
If you wanna have it as yours, please click full size and you will go to page download in full size, so you just  choose the size above the wallpaper that you want in "Download", 
Click it and download the <?php the_title(); ?> Wallpaper.</p>
</div>
<div class="clear"></div>    
<div class="deskrip">
<h4><?php the_title(); ?> Details :</h4>
<div id="images_details"> 
<div>Tittle : <?php the_title(); ?></div>  
<div>Category : <?php the_category(' '); ?></div>
<div><?php the_tags('Tags : ', ', ', '<br />'); ?></div>
<div>Posted : <?php the_time('F j, Y'); ?> at <?php the_time('g:i a'); ?></div>
<div>Viewed : <?php setPostViews(get_the_ID()); ?><?php echo getPostViews(get_the_ID()); ?> view</div>
<div>File type : <?php $args = array( 'post_type' => 'attachment', 'numberposts' => 1, 'post_status' => null, 'post_parent' => $post->ID ); $attachments = get_posts($args); if ($attachments) { foreach ( $attachments as $attachment ) { echo $attachment->post_mime_type; } } ?></div>
<div>File Size : <?php echo attfilesize(); ?></div>
<div>Resolution :  </strong> <?php global $wpdb; $attachment_id = $wpdb->get_var("SELECT ID FROM $wpdb->posts WHERE
post_parent = '$post->ID' AND post_status = 'inherit' AND post_type='attachment' ORDER BY post_date DESC LIMIT 1"); ?>
<?php 
$attachments = get_posts($args);
$image_attributes = wp_get_attachment_image_src( $attachment_id,'full' ); // returns an array
?> 
<?php echo $image_attributes[1] ?> x <?php echo $image_attributes[2] ?> Pixel</br></div>
<div>Total Download : <?php echo rand(15,50); ?> Download</div>
<div class="full_size">
<a target="_blank" title="Full Size <?php the_title(); ?> " href="<?php $attachments = get_children( array('post_parent' => get_the_ID(), 'post_type' => 'attachment', 'post_mime_type' => 'image') ); foreach ( $attachments as $attachment_id => $attachment ) { echo '' . get_attachment_link( $attachment_id ) . ''; } ?>">Full Size</a>
</div>		
</div>		
<div class="clear"></div>    
</div>
</div>
</div>
</div>
<?php get_sidebar(); ?> 
<div class="clear"></div>
<div class="random_post">
<?php //randompost
query_posts(array('orderby' => 'rand', 'showposts' => 8));
if (have_posts()) :
while (have_posts()) : the_post(); ?>
<div class="home_post_box">
<a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_post_thumbnail('featured-home'); ?></a>
<div class="infopost">
<h2><strong><?php echo substr(get_the_title(),0,40); ?></strong></h2>
<?php
$img_id = get_post_thumbnail_id($post->ID);
$image = wp_get_attachment_image_src($img_id, $optional_size);
?>
<p><?php echo '' . ($image[1]) . 'x' . ($image[2]); ?> px | <?php echo getPostViews(get_the_ID()); ?> Views</p>
</div>
</div> 
<?php endwhile;
endif; ?>
</div>
<div class="clear"></div>
<?php get_footer(); ?>    